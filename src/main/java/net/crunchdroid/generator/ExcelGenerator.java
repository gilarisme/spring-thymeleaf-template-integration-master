package net.crunchdroid.generator;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.ss.usermodel.Row;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import net.crunchdroid.entity.MasterData;
import net.crunchdroid.repo.MasterDataRepository;

@Component
public class ExcelGenerator {
	
	@Autowired
	private MasterDataRepository masterDataRepo;

	/* Import */
    public void importExcel(MultipartFile file) throws Exception{
    	
    	List<String[]> cellValues = ExcelHandler.extractInfo(file);
    	for (int i = 1; i < cellValues.size(); i++) {
    		MasterData masterData = new MasterData();
    		String[] c = cellValues.get(i);
    		masterData.setJenisBantuan(c[0]);
    	  	masterData.setJenisKegiatan(c[1]);
    	  	masterData.setNilaiKontrak(c[2]);
    	  	masterData.setNoKontrak(c[3]);
    	  	masterData.setTargetVolume(c[4]);
    	  	masterData.setNamaPengawas(c[5]);
    	  	masterData.setEmailPengawas(c[6]);
    	  	masterData.setTitikKoordinat(c[7]);
    	  	masterDataRepo.save(masterData);
		}
    	    	
		}
		  
   

    /* Cout Row of Excel Table */
    public static int CoutRowExcel(Iterator<Row> iterator) {
        int size = 0;
        while(iterator.hasNext()) {
            size++;
        }
        return size;
    }
}
