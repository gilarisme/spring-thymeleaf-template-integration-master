package net.crunchdroid.repo;

import org.springframework.data.repository.CrudRepository;

import net.crunchdroid.entity.MasterData;

public interface MasterDataRepository extends CrudRepository<MasterData, Long> {

}
