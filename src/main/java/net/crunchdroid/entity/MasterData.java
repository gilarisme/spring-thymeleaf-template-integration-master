package net.crunchdroid.entity;

import java.io.Serializable;

import javax.persistence.*;

import lombok.*;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "m_data")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MasterData implements Serializable {
	
	private static final long serialVersionUID = -246760547006423214L;
	
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
	private String jenisBantuan;
    private String jenisKegiatan;
    private String nilaiKontrak;
    private String noKontrak;
    private String targetVolume;
    private String namaPengawas;
    private String emailPengawas;
    private String titikKoordinat;
}
