package net.crunchdroid.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import net.crunchdroid.entity.User;
import net.crunchdroid.generator.ExcelGenerator;


/**
 * @author CrunchDroid
 */
@Controller
public class PageController {
	
	@Autowired
    private ExcelGenerator excel;
	
	
	@RequestMapping(value = {"", "/"}, method = RequestMethod.GET)
    public String index(ModelMap modelMap, @AuthenticationPrincipal User principal) {
        return "redirect:/plain-page";
    }
	
    @RequestMapping(value = {"plain-page"}, method = RequestMethod.GET)
    public String plainPage(ModelMap modelMap, @AuthenticationPrincipal User principal) {
        return "plain-page";
    }
    
    @RequestMapping(value = {"/upload"}, method = RequestMethod.GET)
    public String uploadDataPage(ModelMap modelMap, @AuthenticationPrincipal User principal) {
        return "upload";
    }
    
    @RequestMapping(value = {"/uploadExcel"}, method = RequestMethod.POST)
    public String uploadDataExcel( @RequestParam("Data") MultipartFile file) throws Exception {
    	excel.importExcel(file);

        return "redirect:/upload";
    }

    @GetMapping("/pricing-tables")
    public String pricingTables() {
        return "pricing-tables";
    }
    
    @PostMapping("/generate")
    public String Pos() {
        return "succes";
    }

}
